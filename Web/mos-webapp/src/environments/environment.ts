// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyB_LnHGddUSRpYDgofaOs8SdNv84Ba6qa0",
    authDomain: "mos-project-1515504485829.firebaseapp.com",
    databaseURL: "https://mos-project-1515504485829.firebaseio.com",
    projectId: "mos-project-1515504485829",
    storageBucket: "mos-project-1515504485829.appspot.com",
    messagingSenderId: "662034786425"
  }
};
