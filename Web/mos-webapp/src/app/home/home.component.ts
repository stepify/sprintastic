import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user';
import { FireBaseService } from '../services/firebase.service';
import { JodaTime, RunningActivity } from '../models/runningActivity';

@Component({
  selector: 'home',
  templateUrl: './layout/home.component.html',
  styleUrls: ['./layout/home.component.css']
})
export class HomeComponent {
  //public runs:Run[] = [];
  public user:User;
  
  single: any[] = [
    {
      "name": "November",
      "value": 894000
    },
    {
      "name": "December",
      "value": 500000
    },
    {
      "name": "January",
      "value": 720000
    }
  ];
  multi = [
    {
      "name": "January",
      "series": [
        {
          "name": "2016",
          "value": 730000
        },
        {
          "name": "2017",
          "value": 894000
        }
      ]
    },
  
    {
      "name": "December",
      "series": [
        {
          "name": "2016",
          "value": 430000
        },
        {
          "name": "2017",
          "value": 500000
        }
      ]
    },
  
    {
      "name": "November",
      "series": [
        {
          "name": "2016",
          "value": 544032
        },
        {
          "name": "2017",
          "value": 894000
        }
      ]
    },
  ];
  

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = false;
  showXAxisLabel = true;
  xAxisLabel = 'Month';
  showYAxisLabel = true;
  yAxisLabel = 'Steps';

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private route: ActivatedRoute, private router: Router, public afAuth: AngularFireAuth, private firebaseRef:FireBaseService){
    /*this.runs = [
      new Run(1,826.7,new Date(2018,1,1,10,20,45),new Date(2018,1,1,11,27,53),15.4),
      new Run(2,389,new Date(2018,1,2,9,11,11),new Date(2018,1,2,9,40,9),7.3),
      new Run(3,1458,new Date(2018,1,3,17,1,59),new Date(2018,1,3,18,45,4),22.4),
      new Run(4,456,new Date(2018,1,7,14,7,30),new Date(2018,1,7,14,47,4),9.4),
      new Run(5,256,new Date(2018,1,9,16,36,30),new Date(2018,1,9,16,58,11),5.0)
    ];*/
    firebaseRef.getUser().then(user => {
      this.user=user as User
      if(this.user instanceof User) console.log("success");
    });
    Object.assign(this, this.single)   
  }

  getDateFrom(time:JodaTime):Date {
    return new Date(time.time);
  }

  openDetailForRun(run:RunningActivity){
    this.firebaseRef.switchToDetailForRun(run);
    this.router.navigate(['/detail']);
  }
}