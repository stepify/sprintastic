import { Injectable, ApplicationRef } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFireDatabase, DatabaseReference } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user';
import { RunningActivity } from '../models/runningActivity';
import { LatLng } from '../models/latlng';
import { reject } from 'q';

/*
  Generated class for the FavoritesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FireBaseService {

    private dbReference: any;
    private userRef:DatabaseReference;
    private user:User;
    private run:RunningActivity;

    constructor(db: AngularFireDatabase, public afAuth: AngularFireAuth, app: ApplicationRef) {
        this.userRef = db.database.ref('user/'+afAuth.auth.currentUser.uid);
    }

    public getUser():Promise<User>{
        return new Promise(resolve => {
            if(this.user) {
                resolve(this.user);
            } else {
                this.userRef.on('value', userSnapshot => {
                    this.user = this.castToUser(userSnapshot.val());
                    if(this.user instanceof User){
                        resolve(this.user);
                    } else {
                        reject();
                    }
                });
            }
        });
    }

    public switchToDetailForRun(run:RunningActivity) {
        this.run = run;
    }

    public getDetailRun():RunningActivity {
        return this.run;
    }

    private castToUser(obj:any):User{
        let runs:RunningActivity[] = [];

        for(let run of obj.runningActivities) {
            let route:LatLng[] = [];
            route=run.route;
            let heartrate:number[] = [];
            heartrate.push(run.heartRateInfos)
            runs.push(new RunningActivity(123,run.steps,route,heartrate,run.startTime,run.endTime));
        }

        return new User(obj.firstName,obj.lastName,obj.age,obj.age,obj.height,runs);
    }

}

