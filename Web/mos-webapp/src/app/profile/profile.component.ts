import { Component, ApplicationRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase, DatabaseReference } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user';
import { FireBaseService } from '../services/firebase.service';

@Component({
  selector: 'profile',
  templateUrl: './layout/profile.component.html',
  styleUrls: ['./layout/profile.component.css']
})
export class ProfileComponent {

  user:User;
  showBanner = false;

  constructor (private route: ActivatedRoute, private router: Router, private firebaseRef:FireBaseService) {
    firebaseRef.getUser().then(user => this.user=user);
  }

  /*saveUser() {
    this.userRef.set(this.user, () => {
      this.showBanner = true;
      setTimeout(() => {
        this.showBanner = false;
      },1000);
    })
  }*/
}
