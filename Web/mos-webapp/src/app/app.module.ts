import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import {NgxChartsModule} from '@swimlane/ngx-charts';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { DetailComponent } from './detail/detail.component';

import { AgmCoreModule, GoogleMapsAPIWrapper} from '@agm/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { RegisterComponent } from './register/register.component';
import { AngularFireDatabase } from 'angularfire2/database';
import { RMCalcComponent } from './rmcalc/rm.component';
import { RoundPipe } from './pipes/round.pipe';
import { FireBaseService } from './services/firebase.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    DetailComponent,
    RegisterComponent,
    RMCalcComponent,
    RoundPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    NgxChartsModule, 
    AppRoutingModule,
    ReactiveFormsModule, 
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAsCiEZiQmlQJ0HLcvP6AE8ZTa7a75PJl8'
    }),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
  ],
  providers: [
    GoogleMapsAPIWrapper,
    AngularFireDatabase,
    FireBaseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
