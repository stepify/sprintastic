import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  showRegister = true;

  constructor (private route: ActivatedRoute, private router: Router, public afAuth: AngularFireAuth) {
    
  }
  logOutUser() {
    this.afAuth.auth.signOut();
  }
}
