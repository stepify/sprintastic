import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LatLngBounds, MapsAPILoader } from '@agm/core';
import { RunningActivity } from '../models/runningActivity';
import { LatLng } from '../models/latlng';
import { FireBaseService } from '../services/firebase.service';

declare var google:any;

@Component({
  selector: 'detail',
  templateUrl: './layout/detail.component.html',
  styleUrls: ['./layout/detail.component.css']
})
export class DetailComponent {

    run:RunningActivity = null;
    title: string = 'Details of run number 1';
    runningRoute:LatLng[] = [];
    startPoint:LatLng;
    endPoint:LatLng;
    routeBounds:any;

    constructor(private route: ActivatedRoute, private router: Router, private mapsAPILoader: MapsAPILoader, private firebaseRef:FireBaseService){
        this.run = this.firebaseRef.getDetailRun();
        this.startPoint = this.run.route[0];
        this.endPoint = this.run.route[this.run.route.length-1];

        this.mapsAPILoader.load().then(() => {
            this.routeBounds = new window['google'].maps.LatLngBounds();
            this.run.route.forEach((location) => {
                this.routeBounds.extend(new window['google'].maps.LatLng(location.lat, location.lng))
            })
        });
        
    }
}