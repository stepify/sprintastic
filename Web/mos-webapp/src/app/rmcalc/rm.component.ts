import { Component, ApplicationRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase, DatabaseReference } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'rm-calc',
  templateUrl: './layout/rm.component.html',
  styleUrls: ['./layout/rm.component.css']
})
export class RMCalcComponent {

    reps:number = 10;
    repCount = [2,3,4,5,6,7,8,9,10,11,12];
    weight:number = 50;
    round = false;
    showFormular = false;
    graphData:GraphData[] = [];

    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = true;
    showXAxisLabel = true;
    xAxisLabel = 'Reps';
    showYAxisLabel = true;
    yAxisLabel = 'Weight';
    autoScale = true;
    timeline = true;
    colorScheme = {
        domain: [
            '#2597FB', '#65EBFD', '#99FDD0', '#FCEE4B', '#AAAAAA', '#FDD6E3', '#FCB1A8', '#EF6F7B', '#CB96E8', '#EFDEE0'
        ]
    };

    constructor (private route: ActivatedRoute, private router: Router) {
        this.populateGraphData();
    }

    calcEpley(divisor=30):number {
        let rm = this.weight * (1+(this.reps/divisor));
        return rm;
    }

    calcBrzycki(factor=1,divisor1=1.0278,divisor2=0.0278):number{
        let rm = this.weight*factor / (divisor1 - (divisor2*this.reps));
        return rm;
    }

    calcLombardi(){
        let rm = this.weight * Math.pow(this.reps,0.1);
        return rm;
    }

    calcMayhew(factor=100,divisor1=52.2,divisor2=41.9,expo=-0.055){
        let rm = this.weight*factor / (divisor1 + (divisor2*Math.pow(Math.E,this.reps*expo)));
        return rm;
    }

    calcEpleyReps(reps:number,divisor=30):number{
        let rm = this.calcEpley(divisor);
        let weight = rm / (1 + (reps/divisor));
        return weight; 
    }

    calcBrzyckiReps(reps:number,factor=1,divisor1=1.0278,divisor2=0.0278):number{
        let rm = this.calcBrzycki(factor,divisor1,divisor2);
        let weight = rm * (divisor1 - (divisor2*reps)) / factor;
        return weight;
    }

    calcMayhewReps(reps:number,factor=100,divisor1=52.2,divisor2=41.9,expo=-0.055):number{
        let rm = this.calcMayhew(factor,divisor1,divisor2,expo);
        let weight = rm * (divisor1 + (divisor2*Math.pow(Math.E,expo*reps))) / factor;
        return weight;
    }

    calcLombardiReps(reps:number){
        let rm = this.calcLombardi();
        let weight = rm / (Math.pow(reps,0.1))
        return weight;
    }

    populateGraphData(){
        this.graphData = [];
        var repData:RepData[] = [];

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcEpleyReps(i)});}
        this.graphData.push({"name":"Epley","series":repData});

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcEpleyReps(i,40)});}
        this.graphData.push({"name":"O'Conner","series":repData});

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcBrzyckiReps(i)});}
        this.graphData.push({"name":"Brzycki","series":repData});

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcBrzyckiReps(i,100,101.3,2.67123)});}
        this.graphData.push({"name":"McGlothin","series":repData});

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcMayhewReps(i)});}
        this.graphData.push({"name":"Mayhew","series":repData});

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcMayhewReps(i,100,48.8,53.8,-0.075)});}
        this.graphData.push({"name":"Wathan","series":repData});

        repData = [];
        for(let i = 1; i < 13; i++) {repData.push({"name":i.toString(),"value":this.calcLombardiReps(i)});}
        this.graphData.push({"name":"Lombardi","series":repData});

    }

}

/*
{
    "name": "Germany",
    "series": [
      {
        "name": "2010",
        "value": 7300000
      },
      {
        "name": "2011",
        "value": 8940000
      }
    ]
  },*/

export interface GraphData {
    name: string;
    series: RepData[]; 
}

export interface RepData{
    name: string;
    value: number;
}