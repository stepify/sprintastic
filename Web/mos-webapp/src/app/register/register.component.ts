import { Component, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebase } from '@firebase/app';
import { User } from '../models/user';

@Component({
  selector: 'register',
  templateUrl: './layout/register.component.html',
  styleUrls: ['./layout/register.component.css']
})
export class RegisterComponent {

  @Output() loginClicked = new EventEmitter();

  public user:User;
  public email:string;
  public password:string;

  constructor (private route: ActivatedRoute, private router: Router, public afAuth: AngularFireAuth) {
    
  }

  public registerUser() {
    this.afAuth.auth.createUserWithEmailAndPassword(this.email, this.password);
    this.loginClicked.emit();
  }

  public loginBtnClicked(){
    this.loginClicked.emit();
  }
}
