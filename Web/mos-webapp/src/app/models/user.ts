import { RunningActivity } from "./runningActivity";

export class User {
    constructor (private firstName:string, private lastName:string, private age:number, private weight:number, private height:number, private runningActivities:RunningActivity[]) {
    }
    
}