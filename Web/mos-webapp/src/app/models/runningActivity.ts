import { LatLng } from "./latlng";

export class RunningActivity {
    private startTime:Date;
    private endTime:Date;

    constructor(private distance:number,private steps:number, public route:LatLng[], private heartRateInfo:number[], startTimeTemp:JodaTime,endTimeTemp:JodaTime){
        this.castToStartAndEndTime(startTimeTemp.time,endTimeTemp.time);
    }

    private castToStartAndEndTime(startTime:number, endTime:number) {
        this.startTime = new Date(startTime);
        this.endTime = new Date(endTime);
    }

    public getDuration():string{
        var seconds = (this.endTime.getTime() - this.startTime.getTime())/1000;
        let hours = seconds / 3600;
        seconds = seconds % 3600;
        let minutes = seconds / 60;
        seconds = seconds % 60;
        //seconds = seconds / 60;
        
        return Math.trunc(hours)+":"+Math.round(minutes)+":"+Math.round(seconds);
    }

    public getPace():string {
        //let minutes = (this.endTime.getTime() - this.startTime.getTime())/1000/60;
        //return (minutes/this.distance).toFixed(2);
        let paceTime = (this.endTime.getTime() - this.startTime.getTime()) / this.distance;
        var paceSecs = paceTime/1000;
        let hours = paceSecs / 3600;
        paceSecs = paceSecs % 3600;
        let minutes = paceSecs / 60;
        paceSecs = paceSecs % 60;
        
        return Math.trunc(hours)+":"+Math.round(minutes)+":"+Math.round(paceSecs);
        
    }
}

export interface JodaTime {
    date:number
    day:number
    hours:number;
    minutes:number;
    month:number;
    seconds:number;
    time:number;
    timezoneOffset:number;
    year:number;
}