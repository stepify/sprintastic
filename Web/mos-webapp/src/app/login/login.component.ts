import { Component, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import { firebase } from '@firebase/app';

@Component({
  selector: 'login',
  templateUrl: './layout/login.component.html',
  styleUrls: ['./layout/login.component.css']
})
export class LoginComponent {
  
  @Output() registerClicked = new EventEmitter();

  public email:string;
  public password:string;

  constructor (private route: ActivatedRoute, private router: Router, public afAuth: AngularFireAuth) {
    
  }

  public signIn(){
    this.afAuth.auth.signInWithEmailAndPassword(this.email,this.password);
  }

  public registerBtnClicked(){
    this.registerClicked.emit();
  }
}
