package at.debugged.ms.exercise01.stepdetection;

import java.util.ArrayList;
import java.util.HashMap;

import at.debugged.ms.exercise01.model.AccelerometerData;

/**
 * Created by Martin on 09.11.2017.
 */

public class AccelerometerDataManager implements AxisEvaluation.AxisStepDetectionListener {

    public interface StepListener{
        void stepDetected(int totalSteps);
    }

    private static ArrayList<StepListener> mStepListener = new ArrayList<>();

    public static final float SAMPLE_PRECISION =5f;
    public static final float STEP_PRECISION =0.3f;
    public static final int STEP_COUNT_INTERVAL_MILLI = 500;

    private static final int X_AXIS = 0;
    private static final int Y_AXIS = 1;
    private static final int Z_AXIS = 2;

    private HashMap<Integer,Float> mLastStepChanges = new HashMap<>();

    private AxisEvaluation mXAxis = new AxisEvaluation(X_AXIS,this);
    private AxisEvaluation mYAxis = new AxisEvaluation(Y_AXIS,this);
    private AxisEvaluation mZAxis = new AxisEvaluation(Z_AXIS,this);

    private int mSteps = 0;
    private long mTimestampLastStep = 0;

    public static void addStepListener(StepListener stepListener){
        mStepListener.add(stepListener);
    }

    public void addNewSensorValues(AccelerometerData data){
        mXAxis.addNewSensorValues(data.getX());
        mYAxis.addNewSensorValues(data.getY());
        mZAxis.addNewSensorValues(data.getZ());
    }

    @Override
    public void stepDetected(int id, float accChange) {
        mLastStepChanges.put(id,accChange);

        if(mLastStepChanges.size() == 3){
            if (getLargestChange() > STEP_PRECISION &&
                System.currentTimeMillis() - mTimestampLastStep> STEP_COUNT_INTERVAL_MILLI){
                mSteps++;
                for(StepListener listener : mStepListener) {
                    listener.stepDetected(mSteps);
                }
                mTimestampLastStep = System.currentTimeMillis();
            }
            mLastStepChanges = new HashMap<>();
        }
    }

    private float getLargestChange(){

        return Math.max(mLastStepChanges.get(X_AXIS),
                Math.max(mLastStepChanges.get(Y_AXIS), mLastStepChanges.get(Z_AXIS)));

    }
}
