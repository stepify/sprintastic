package at.debugged.ms.exercise01.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Messenger;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import at.debugged.ms.exercise01.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StepFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StepFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StepFragment extends Fragment implements SensorEventListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public final static String ARG_STEPS = "steps";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private SharedPreferences mSharedPref;
    private int mProgressCircleStatus = 0;
    private int mProgressStatus1 = 0;
    private int mProgressStatus2 = 0;
    private int mProgressStatus3 = 0;
    private int mProgressStatus4 = 0;
    private Handler handler = new Handler();
    private int mSteps = 0;
    private int mSteps1 = 0;
    private int mSteps2 = 0;
    private int mSteps3 = 0;
    private int mSteps4 = 0;
    private int mAndroidSteps = 0;

    private Messenger mActivityMessenger;

    private SensorManager mSensorManager;

    @BindView(R.id.pb)
    ProgressBar mPb_circle;
//    public ProgressBar mPb_circle = null;

    @BindView(R.id.tv)
    TextView mTv_circle;
//    public TextView mTv_circle = null;

    @BindView(R.id.text_bar_1)
    TextView text_bar_1;
    @BindView(R.id.text_bar_2)
    TextView text_bar_2;
    @BindView(R.id.text_bar_3)
    TextView text_bar_3;
    @BindView(R.id.text_bar_4)
    TextView text_bar_4;
    @BindView(R.id.bar_1)
    ProgressBar bar_1;
    @BindView(R.id.bar_2)
    ProgressBar bar_2;
    @BindView(R.id.bar_3)
    ProgressBar bar_3;
    @BindView(R.id.bar_4)
    ProgressBar bar_4;

    @BindView(R.id.text_daily_steps)
    TextView dailyGoal;

    @BindView(R.id.tv_gsc_text)
    TextView mGsdText;


    public StepFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StepFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StepFragment newInstance(String param1, String param2) {
        StepFragment fragment = new StepFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        mSensorManager = (SensorManager) getActivity().getSystemService(Context.SENSOR_SERVICE);
//        mActivityMessenger = new Messenger(mHandler);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_step, container, false);

        ButterKnife.bind(this, view);

        mSharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);

        int stepGoal = mSharedPref.getInt(getString(R.string.preference_set_step_goal), 0);
        dailyGoal.setText(getString(R.string.string_daily_goal) + " " + stepGoal + " " + getString(R.string.string_daily_steps));
        int steps = mSharedPref.getInt(getString(R.string.preference_step_detected), 0);

        setupProgress(stepGoal, steps);

        mTv_circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StepDialog dialogFragment = new StepDialog();
                dialogFragment.show(getFragmentManager(), "Steps_goal");
                int stepGoal = mSharedPref.getInt(getString(R.string.preference_set_step_goal), 0);
//              dailyGoal.setText(getString(R.string.string_daily_goal)+ stepGoal + getString(R.string.string_daily_steps));

            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        mAndroidSteps += sensorEvent.values.length;
        Log.i("TAG", "Andoird Steps: " + mAndroidSteps);
        mGsdText.setText(getString(R.string.android_step_detection) + mAndroidSteps);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    public void updateCircleBar(final int steps, int stepgoal) {

        final int percentage = (int) (((double) 100 / (double) stepgoal) * (double) steps);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mProgressCircleStatus < percentage) {
                    mProgressCircleStatus += 1;

                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void run() {
                            mPb_circle.setProgress(mProgressCircleStatus);
                            mTv_circle.setText(steps + "");
                            if (mProgressCircleStatus == 100) {
                                mTv_circle.setText("Complete");
                            }
                        }
                    });
                }
            }
        }).start();
    }

    public void setupProgress(int StepGoal, int Steps) {
        final int mStepGoal = StepGoal;
        mSteps = Steps;

        final int percentage = (int) (((double) 100 / (double) mStepGoal) * (double) mSteps);
//        Log.i(TAG, "Percantage: " + percentage);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mProgressCircleStatus < percentage) {
                    mProgressCircleStatus += 1;

                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void run() {
                            mPb_circle.setProgress(mProgressCircleStatus);
                            mTv_circle.setText(mSteps + "");
                            if (mProgressCircleStatus == 100) {
                                mTv_circle.setText("Complete");
                            }
                        }
                    });
                }
            }
        }).start();


        mSteps1 = 325;
        final int percentageBar1 = (int) (((double) 100 / (double) mStepGoal) * (double) mSteps1);

        new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus1 < percentageBar1) {
                    mProgressStatus1 += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        @SuppressLint("SetTextI18n")
                        public void run() {
                            bar_1.setProgress(mProgressStatus1);
                            text_bar_1.setText(mSteps1 + "/" + mStepGoal);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        mSteps2 = 650;
        final int percentageBar2 = (int) (((double) 100 / (double) mStepGoal) * (double) mSteps2);

        new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus2 < percentageBar2) {
                    mProgressStatus2 += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        @SuppressLint("SetTextI18n")
                        public void run() {
                            bar_2.setProgress(mProgressStatus2);
                            text_bar_2.setText(mSteps2 + "/" + mStepGoal);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        mSteps3 = 975;
        final int percentageBar3 = (int) (((double) 100 / (double) mStepGoal) * (double) mSteps3);

        new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus3 < percentageBar3) {
                    mProgressStatus3 += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        @SuppressLint("SetTextI18n")
                        public void run() {
                            bar_3.setProgress(mProgressStatus3);
                            text_bar_3.setText(mSteps3 + "/" + mStepGoal);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        mSteps4 = 100;
        final int percentageBar4 = (int) (((double) 100 / (double) mStepGoal) * (double) mSteps4);

        new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus4 < percentageBar4) {
                    mProgressStatus4 += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        @SuppressLint("SetTextI18n")
                        public void run() {
                            bar_4.setProgress(mProgressStatus4);
                            text_bar_4.setText(mSteps4 + "/" + mStepGoal);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
//

    }

    @Override
    public void onResume() {
        super.onResume();
        Sensor countSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (countSensor != null) {
            mSensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_UI);
        } else {

        }
    }
}
