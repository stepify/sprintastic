package at.debugged.ms.exercise01.ui;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;
import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.Locale;

import at.debugged.ms.exercise01.Global;
import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.DomainNames;
import at.debugged.ms.exercise01.model.WayPoint;
import at.debugged.ms.exercise01.model.domain.HeartRateInfo;
import at.debugged.ms.exercise01.model.domain.RunningActivity;
import at.debugged.ms.exercise01.services.BLEService;
import at.debugged.ms.exercise01.services.PositioningService;
import at.debugged.ms.exercise01.stepdetection.AccelerometerDataManager;
import at.debugged.ms.exercise01.utils.PermissionUtils;
import at.debugged.ms.exercise01.utils.UnitConverter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static at.debugged.ms.exercise01.Tags.ACTIVITY_MESSENGER_EXTRA;
import static at.debugged.ms.exercise01.Tags.HEART_RATE_CHANGED;
import static at.debugged.ms.exercise01.Tags.LOCATION_NOT_AVAILABLE;
import static at.debugged.ms.exercise01.Tags.LOCATION_UPDATE_RECEIVED;
import static at.debugged.ms.exercise01.Tags.UPDATE_ACTIVITY_MESSENGER;

public class WorkoutActivity extends AppCompatActivity implements OnMapReadyCallback, AccelerometerDataManager.StepListener {
    private static final int REQUEST_PERMISSIONS_CODE = 1;
    private static final int ROUTE_PADDING_DP = 12;
    private static final String DISTANCE_UNIT = "km";   //TODO: Create a class for metric configs
    private static final String SPEED_UNIT = "km/h";

    public static final String TAG = "WorkoutActivity";

    private Messenger mActivityMessenger;
    private GoogleMap mMap;

    private RunningActivity currentRunningActivity;
    private boolean mIsRunning = false;

    private WayPoint mLastWayPoint = null;

    private double mStrideLength = 0d;
    private boolean mUseStrideForDistance = false;

    @BindView(R.id.tv_start_time)
    TextView mTvStartTime;

    @BindView(R.id.tv_heart_rate)
    TextView mTvHeartRate;

    @BindView(R.id.tv_duration)
    TextView mTvDuration;

    @BindView(R.id.tv_distance)
    TextView mTvDistance;

    @BindView(R.id.tv_pace)
    TextView mTvPace;

    @BindView(R.id.tv_steps)
    TextView mTvSteps;

    @BindView(R.id.tv_VO2max)
    TextView mTvVO2max;

    @BindView(R.id.tv_energy_expenditure)
    TextView mTvEnergyExpenditure;

    @BindView(R.id.tv_energy_expenditure_kcal)
    TextView getmTvEnergyExpenditureKcal;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.d(TAG, "Message received: " + msg);

            switch (msg.what) {
                case UPDATE_ACTIVITY_MESSENGER:
                    Messenger mServiceMessenger = msg.replyTo;

                    Message replyMsg = Message.obtain();
                    replyMsg.what = UPDATE_ACTIVITY_MESSENGER;

                    try {
                        mServiceMessenger.send(replyMsg);
                    } catch (RemoteException _e) {
                        _e.printStackTrace();
                    }
                    break;

                case HEART_RATE_CHANGED:
                    int heartRate = msg.arg1;
                    mTvHeartRate.setText(String.valueOf(heartRate));
                    currentRunningActivity.addHeartRAteInfo(new HeartRateInfo(heartRate, new DateTime()));
                    energyExpenditure(heartRate);
                    break;

                case LOCATION_UPDATE_RECEIVED:
                    if (mIsRunning) {
                        currentRunningActivity.addWayPoints((LinkedList<WayPoint>) msg.obj);
                        showRoute();
                        calculateCurrentDistance((LinkedList<WayPoint>) msg.obj);
                        displayPace();
                    }
                    break;

                case LOCATION_NOT_AVAILABLE:
                    mUseStrideForDistance = true;
            }

            return true;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        ButterKnife.bind(this);
        clearUI();

        currentRunningActivity = new RunningActivity();
        mActivityMessenger = new Messenger(mHandler);

        AccelerometerDataManager.addStepListener(this);

        mStrideLength = calculateStrideLength();

        checkPermissions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.workout_options_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_connect_heart_reate:
                startActivityForResult(new Intent(this, HrDeviceOverviewActivity.class), 1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        stopService(new Intent(this, PositioningService.class));
        stopService(new Intent(this, BLEService.class));
        super.onDestroy();
    }

    private void checkPermissions() {
        // Todo: ask for permissions more detailed
        // Todo: check bluetooth enabled

        if (!PermissionUtils.checkBluetoothAdmin(this) || !PermissionUtils.checkFineLocation(this) || !PermissionUtils.checkInternet(this)) {
            String[] requestPermissions = {
                    Manifest.permission.BLUETOOTH_ADMIN,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.INTERNET
            };

            Log.d(TAG, "Start Service - request permissions");
            ActivityCompat.requestPermissions(this, requestPermissions, REQUEST_PERMISSIONS_CODE);
        } else if (PermissionUtils.checkFineLocation(this)) {
            startLocationService(); //TODO: Maybe move this one
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS_CODE:
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Permissions have to be granted, otherwise app won't work.", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PERMISSIONS_CODE && resultCode == Activity.RESULT_OK) {
            BluetoothDevice device = data.getParcelableExtra("selected_hr_sensor");
            Log.d(TAG, "Starting " + device.getAddress());
            startBLEServiceWithPermissions(device);
        }
    }

    private void startBLEServiceWithPermissions(BluetoothDevice device) {
        Intent startServiceIntent = new Intent(this, BLEService.class);
        startServiceIntent.putExtra(ACTIVITY_MESSENGER_EXTRA, mActivityMessenger);
        startServiceIntent.putExtra("hr_sensor", device);

        startService(startServiceIntent);

        Toast.makeText(this, "Service started", Toast.LENGTH_SHORT).show();
    }

    private void startLocationService() {
        Intent serviceIntent = new Intent(this, PositioningService.class);
        serviceIntent.putExtra(ACTIVITY_MESSENGER_EXTRA, mActivityMessenger);

        startService(serviceIntent);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // center in austria
        LatLngBounds.Builder geoBox = LatLngBounds.builder();

        geoBox.include(new LatLng(49.019517, 15.010163));
        geoBox.include(new LatLng(48.017138, 17.168977));
        geoBox.include(new LatLng(46.374999, 14.565217));
        geoBox.include(new LatLng(47.500151, 9.544466));

        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(geoBox.build(), 50));
        mMap.getUiSettings().setAllGesturesEnabled(false);
    }

    private synchronized void showRoute() {
        if (currentRunningActivity.getRoute().size() >= 2) {
            mMap.clear();

            LatLngBounds.Builder geoBox = LatLngBounds.builder();
            PolylineOptions routePolyline = new PolylineOptions().geodesic(true).color(ContextCompat.getColor(this, R.color.colorPrimary));
            routePolyline.width(getResources().getInteger(R.integer.polyline_width));

            for (WayPoint point : currentRunningActivity.getRoute()) {
                LatLng latlng = point.toLatLng();
                routePolyline.add(latlng);
                geoBox.include(latlng);
            }

            mMap.addPolyline(routePolyline);

            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                    geoBox.build(),
                    (int) UnitConverter.dpToPx(this, ROUTE_PADDING_DP))
            );
        }
    }

    private synchronized void calculateCurrentDistance(LinkedList<WayPoint> _newPoints) {

        if (mLastWayPoint != null) {   // Consider distance to last calculation
            _newPoints.addFirst(mLastWayPoint);
        } else {
            mLastWayPoint = _newPoints.getFirst();
        }

        if (_newPoints.size() > 1) {
            double distance = currentRunningActivity.getDistance();

            for (int i = 0; i < _newPoints.size() - 1; i++) {  //There can be multiple new locations
                float result[] = new float[3];  //Result is stored in this array. idx 0 => distance
                WayPoint wayPoint1 = _newPoints.get(i);
                WayPoint wayPoint2 = _newPoints.get(i + 1);
                Location.distanceBetween(wayPoint1.getLat(), wayPoint1.getLng(),
                        wayPoint2.getLat(), wayPoint2.getLng(), result);
                distance += result[0];
                currentRunningActivity.setDistance(distance);
            }
            displayDistance();
            mLastWayPoint = _newPoints.getLast();
        }
    }

    private void displayDistance() {
        double distance = currentRunningActivity.getDistance();
        double km = distance / 1000;
        DecimalFormat df = new DecimalFormat("#.##");

        mTvDistance.setText(df.format(km) + " " + DISTANCE_UNIT);
    }

    private void displayPace() {
        double distance = currentRunningActivity.getDistance();
        double milli1 = new DateTime().getMillis();
        double milli2 = currentRunningActivity.getStartDateTime().getMillis();
        double seconds = (milli1 - milli2) / 1000;

        double pace = (distance / seconds) * 3.6;
        DecimalFormat df = new DecimalFormat("#.##");

        mTvPace.setText(df.format(pace) + " " + SPEED_UNIT);
    }

    @OnClick(R.id.btn_control_activity)
    public void onControlActivityButtonClicked(View view) {
        FloatingActionButton button = (FloatingActionButton) view;

        if (!mIsRunning) {
            // start activity
            currentRunningActivity = new RunningActivity();
            currentRunningActivity.setStartDateTime(new DateTime());
            DateTimeFormatter dtf = DateTimeFormat.shortTime();
            mTvStartTime.setText(dtf.print(currentRunningActivity.getStartDateTime()));

            button.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_stop_24dp));
            //startLocationService();
            timerHandler.post(timerRunnable);
            mIsRunning = true;

        } else {
            // stop activity
            currentRunningActivity.setEndDateTime(new DateTime());
            Global.currentUser.addRunningActivity(currentRunningActivity);

            button.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_play_arrow_black_24dp));
            stopService(new Intent(this, BLEService.class));
            stopService(new Intent(this, PositioningService.class));
            timerHandler.removeCallbacks(timerRunnable);

            mIsRunning = false;
            clearUI();
            saveToDatabase();
            finish();
        }
    }

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            Period period = new Period(currentRunningActivity.getStartDateTime(), new DateTime());
            mTvDuration.setText(String.format(Locale.getDefault(), "%02d:%02d:%02d", period.getHours(), period.getMinutes(), period.getSeconds()));
            timerHandler.postDelayed(this, 1000);
        }
    };

    private void saveToDatabase() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userReference = database.getReference(DomainNames.USER);

        userReference.child(auth.getCurrentUser().getUid()).setValue(Global.currentUser);
    }

    private void clearUI() {
        mTvHeartRate.setText("--");
        mTvVO2max.setText("--");
        mTvEnergyExpenditure.setText("--");
        getmTvEnergyExpenditureKcal.setText("--");
    }

    //One Stride = Two Steps
    private double calculateStrideLength() {

        int gender = Global.currentUser.getGender();
        int age = Global.currentUser.getAge();
        double height = Global.currentUser.getHeight();
        double weight = Global.currentUser.getWeight();

        if (gender != 0) {
            return -0.001 * age + 1.058 * height - 0.002 * weight - 0.129;
        } else {
            double val = -0.002 * age + 0.760 * height - 0.001 * weight + 0.327;
            return val;
        }
    }

    @Override
    public void stepDetected(int totalSteps) {
        if (mIsRunning) {
            long steps = currentRunningActivity.getSteps() + 1;
            mTvSteps.setText(Long.toString(steps));
            currentRunningActivity.setSteps(steps);

            if (mUseStrideForDistance) {
                currentRunningActivity.setDistance((steps / 2) * mStrideLength);
            }
            displayDistance();
            displayPace();
        }
    }

    public void energyExpenditure(int heartRate) {

        double weight = Global.currentUser.getWeight();
        float age = Global.currentUser.getWeight();

        double EE = (-59.3954) + Global.currentUser.getGender() * (-36.3781 + 0.271 * age + 0.394 * weight + 0.404 * VO2max() + 0.634 * heartRate) + (1 - Global.currentUser.getGender()) * (0.274 * age + 0.103 * weight + 0.380 * VO2max() + 0.450 * heartRate);

        mTvEnergyExpenditure.setText(String.format(Locale.getDefault(), "%1$.2f", EE));

        EE = EE/4.168;

        getmTvEnergyExpenditureKcal.setText(String.format(Locale.getDefault(), "%1$.2f", EE));
        currentRunningActivity.addCalorieInfo(EE);
    }

    public double VO2max() {
        double VO2;

        int age = Global.currentUser.getAge();
        int gender = Global.currentUser.getGender();
        double height = Global.currentUser.getHeight() / 100;
        double weight = Global.currentUser.getWeight();
        int par = 7;

        VO2 = (0.133 * age) - (0.005 * age * age) + (11.403 * gender) + (1.463 * par) + (9.17 * height) - (0.254 * weight) + 34.143;
        currentRunningActivity.addVo2MaxInfo(VO2);

        mTvVO2max.setText(String.format(Locale.getDefault(), "%1$.2f", VO2));


        return VO2;
    }
}
