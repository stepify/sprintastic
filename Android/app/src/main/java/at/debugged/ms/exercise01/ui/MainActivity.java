package at.debugged.ms.exercise01.ui;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import at.debugged.ms.exercise01.Global;
import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.AccelerationListener;
import at.debugged.ms.exercise01.model.AccelerometerData;
import at.debugged.ms.exercise01.model.DomainNames;
import at.debugged.ms.exercise01.model.domain.User;
import at.debugged.ms.exercise01.recorder.SensorRecorder;
import at.debugged.ms.exercise01.stepdetection.AccelerometerDataManager;
import at.debugged.ms.exercise01.stepdetection.AccelerometerFactory;
import at.debugged.ms.exercise01.stepdetection.accelerometer.Accelerometer;
import butterknife.BindView;
import butterknife.ButterKnife;

import static at.debugged.ms.exercise01.stepdetection.AccelerometerFactory.SENSOR_TYPE_HARDWARE;

public class MainActivity extends AppCompatActivity implements AccelerationListener, AccelerometerDataManager.StepListener {

    private static final String TAG = "MainActivity";

    private AccelerometerDataManager mDataManager;
    private SensorRecorder mSensorRecorder;
    private boolean mIsRecording = false;

    private Fragment mFragment;
    private FragmentManager mFragmentManager;

    private SharedPreferences mSharedPref;
    private SharedPreferences.Editor mEditor;

    @BindView(R.id.navigation)
    BottomNavigationView mBottomNavView;

    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);

        mSharedPref = getPreferences(Context.MODE_PRIVATE);

        mDataManager = new AccelerometerDataManager();
        mDataManager.addStepListener(this);
        mFragmentManager = getSupportFragmentManager();

        mBottomNavView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.steps:
                        mFragment = new StepFragment();
                        break;
                    case R.id.activity:
                        mFragment = new ActivityOverviewFragment();
                        break;
                    case R.id.settings:
                        mFragment = new SettingsFragment();
                        break;
                }

                final FragmentTransaction transaction = mFragmentManager.beginTransaction();
                transaction.replace(R.id.content, mFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                return true;
            }
        });

        mBottomNavView.setSelectedItemId(R.id.steps);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userReference = database.getReference(DomainNames.USER).child(mAuth.getCurrentUser().getUid());

        // Attach a listener to read the data at our posts reference
        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Global.currentUser = dataSnapshot.getValue(User.class);

                if (Global.currentUser == null) {
                    Global.currentUser = new User();

                    mBottomNavView.setSelectedItemId(R.id.settings);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });

        Accelerometer mAccelerometer = AccelerometerFactory.getAccelerometer(SENSOR_TYPE_HARDWARE, this);

        if (mAccelerometer != null) {
            mAccelerometer.addListener(this);
            mAccelerometer.start();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser == null) {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_logout:
                mAuth.signOut();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
                return true;

            case R.id.start_accelerometer_recording:
                startRecording();
                return true;

            case R.id.stop_accelerometer_recording:
                stopRecording();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void startRecording() {
        mIsRecording = true;
        mSensorRecorder = new SensorRecorder(this);
        mSensorRecorder.start();
    }

    public void stopRecording() {
        mIsRecording = false;
        if (mSensorRecorder != null) {
            mSensorRecorder.stop();
        }
    }

    @Override
    public void onAccelerationChanged(AccelerometerData data) {
        if (!mIsRecording) mDataManager.addNewSensorValues(data);

    }

    @Override
    public void stepDetected(int totalSteps) {
        int SPSteps = mSharedPref.getInt(getString(R.string.preference_step_detected), 0);
        if(totalSteps == 1) SPSteps = 0;

//        totalSteps+=SPSteps;

        mEditor = mSharedPref.edit();
        mEditor.putInt(getString(R.string.preference_step_detected),totalSteps);
        mEditor.commit();
        int stepGoal = mSharedPref.getInt(getString(R.string.preference_set_step_goal), 0);

        StepFragment frag = (StepFragment) mFragmentManager.findFragmentById(R.id.step_fragment);
        if(frag != null){
            frag.updateCircleBar(totalSteps,stepGoal);
        }else {
            StepFragment newFragment = new StepFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            // Replace whatever is in the fragment_container view with this mFragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(R.id.step_fragment, newFragment);
            transaction.addToBackStack(null);
            newFragment.updateCircleBar(totalSteps,stepGoal);

            // Commit the transaction
            //transaction.commit();
        }
    }

}