package at.debugged.ms.exercise01.services;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import at.debugged.ms.exercise01.model.domain.User;

public class DataService {
    private static DataService instance;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;

    public static DataService getInstance() {
        if (instance == null) {
            instance = new DataService();
            instance.mAuth = FirebaseAuth.getInstance();
            instance.mDatabase = FirebaseDatabase.getInstance();
        }

        return instance;
    }

    public User getCurrentUser() {
        return null;
    }

    public void saveUser(User user) {

    }
}
