package at.debugged.ms.exercise01.model;

import java.io.Serializable;

public class AccelerometerData implements Serializable {
    private float mX;
    private float mY;
    private float mZ;

    public float getX() {
        return mX;
    }

    public void setX(float x) {
        this.mX = x;
    }

    public float getY() {
        return mY;
    }

    public void setY(float y) {
        this.mY = y;
    }

    public float getZ() {
        return mZ;
    }

    public void setZ(float z) {
        this.mZ = z;
    }
}
