package at.debugged.ms.exercise01.stepdetection;

import android.content.Context;

import at.debugged.ms.exercise01.stepdetection.accelerometer.DummyAccelerometer;
import at.debugged.ms.exercise01.stepdetection.accelerometer.HardwareAccelerometer;
import at.debugged.ms.exercise01.stepdetection.accelerometer.Accelerometer;

public class AccelerometerFactory {

    public static final int SENSOR_TYPE_DUMMY = 1;
    public static final int SENSOR_TYPE_HARDWARE = 2;

    public static Accelerometer getAccelerometer(int sensorType, Context context) {
        switch (sensorType) {
            case SENSOR_TYPE_DUMMY:
                return new DummyAccelerometer(context);

            case SENSOR_TYPE_HARDWARE:
                return new HardwareAccelerometer(context);
        }

        return null;
    }
}
