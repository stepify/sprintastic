package at.debugged.ms.exercise01.model.domain;

import com.google.firebase.database.Exclude;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by micha on 1/30/2018.
 */

public class HeartRateInfo {
    private int value;
    private Date timestamp;

    public HeartRateInfo() {
        // required empty constructor
    }

    public HeartRateInfo(int value, DateTime timestamp) {
        this.value = value;
        this.timestamp = timestamp.toDate();
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Exclude
    public DateTime getTimestamp() {
        return new DateTime(timestamp);
    }

    public void setTimestamp(DateTime timestamp) {
        this.timestamp = timestamp.toDate();
    }
}
