package at.debugged.ms.exercise01.model;

/**
 * Created by micha on 11/16/2017.
 */

public interface AccelerationListener {
    void onAccelerationChanged(AccelerometerData data);
}
