package at.debugged.ms.exercise01;

public class Tags {
    public static final String ACTIVITY_MESSENGER_EXTRA = "ActivityMessengerExtra";
    public static final String SELECTED_MAC_ADDRESS = "selectedMacAddress";
    public static final int UPDATE_ACTIVITY_MESSENGER = 1;
    public static final int DEVICE_SELECTED_MESSAGE = 2;
    public static final int HEART_RATE_CHANGED = 3;
    public static final int LOCATION_UPDATE_RECEIVED = 4;
    public static final int LOCATION_NOT_AVAILABLE = 5;
    public static final String PREF_SERVICE_ALIVE = "SERVICE_ALIVE";
}