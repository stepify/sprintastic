package at.debugged.ms.exercise01.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.LinkedList;

import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.WayPoint;
import at.debugged.ms.exercise01.ui.WorkoutActivity;

import static at.debugged.ms.exercise01.Tags.ACTIVITY_MESSENGER_EXTRA;
import static at.debugged.ms.exercise01.Tags.LOCATION_NOT_AVAILABLE;
import static at.debugged.ms.exercise01.Tags.LOCATION_UPDATE_RECEIVED;

/**
 * Created by Martin on 28.01.2018.
 */

public class PositioningService extends Service {

    private static final String TAG = "LocationService";
    private static final int LOCATION_INTERVAL = 4000;
    private static final int LOCATION_FASTEST_INTERVAL = 3000;
    private static final int REQUEST_CODE = 789;

    private FusedLocationProviderClient mFusedLocationClient;
    private Messenger mCallback = null;

    private LocationRequest mLocationRequest;

    @Nullable
    @Override
    public IBinder onBind(Intent _intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent _intent, int _flags, int _startId) {
        if (_intent != null) {
            mCallback = _intent.getParcelableExtra(ACTIVITY_MESSENGER_EXTRA);
        }

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        this.mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        this.createLocationRequest();
        checkIfLocationsEnabled();


        if (this.checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient.requestLocationUpdates(mLocationRequest, createLocationCallback(),
                    null);
        }
        super.onCreate();
    }

    private void checkIfLocationsEnabled(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse _locationSettingsResponse) {
                if (_locationSettingsResponse.getLocationSettingsStates().isGpsPresent() && _locationSettingsResponse
                        .getLocationSettingsStates().isGpsUsable()) {
                    startAsForegroundService();
                } else {
                    notifyLocationsNotAvailable();
                    stopSelf();
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception _e) {
                notifyLocationsNotAvailable();
                stopSelf();
            }
        });

    }

    private void startAsForegroundService() {
        Intent notificationIntent = new Intent(this, WorkoutActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, REQUEST_CODE,
                notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_gps)
                .setContentTitle("MOS Activity")
                .setContentText("Come on, be faster!")
                .setContentIntent(pendingIntent).build();

        startForeground(6969, notification);
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(LOCATION_FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private LocationCallback createLocationCallback() {
        LocationCallback callback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult _locationResult) {
                LinkedList<WayPoint> locations = new LinkedList<>();
                for (Location location : _locationResult.getLocations()) {
                    locations.add(new WayPoint(location));
                }

                Message msg = Message.obtain();
                msg.what = LOCATION_UPDATE_RECEIVED;
                msg.obj = locations;

                if (mCallback != null) {
                    try {
                        mCallback.send(msg);
                    } catch (RemoteException _e) {
                        Log.e(TAG, "Error sending message");
                    }
                }

                super.onLocationResult(_locationResult);
            }
        };
        return callback;
    }

    private void notifyLocationsNotAvailable(){
        Message msg = Message.obtain();
        msg.what = LOCATION_NOT_AVAILABLE;

        if (mCallback != null) {
            try {
                mCallback.send(msg);
            } catch (RemoteException _e) {
                Log.e(TAG, "Error sending message");
            }
        }
    }
}
