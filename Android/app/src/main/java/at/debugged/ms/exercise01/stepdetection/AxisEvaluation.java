package at.debugged.ms.exercise01.stepdetection;

import java.util.ArrayList;

import at.debugged.ms.exercise01.stepdetection.AccelerometerDataManager;

/**
 * Created by Martin on 09.11.2017.
 */

public class AxisEvaluation {

    public interface AxisStepDetectionListener{
        void stepDetected(int id, float accChange);
    }

    public AxisEvaluation(int id, AxisStepDetectionListener callback){
        this.mID = id;
        this.mDetectionCallback = callback;
    }

    private int mID;
    private int mValuesSinceLastThreshold = 0;

    private final int MAX_SAMPLES = 50;
    private final int BUFFER_REGISTER_SIZE = 4;

    private ArrayList<Float> mValues = new ArrayList<>();
    private ArrayList<Float> mBufferRegister = new ArrayList<>();

    private float[] mMinMax ={Float.MAX_VALUE,Float.MIN_VALUE};
    private Float mThreshold = null;

    private Float mSampleNew = 0f;
    private Float mSampleOld = 0f;

    private AxisStepDetectionListener mDetectionCallback = null;

    public void addNewSensorValues(float data){
        mBufferRegister.add(0,data);
        if(mBufferRegister.size()>BUFFER_REGISTER_SIZE){
            mBufferRegister.remove(BUFFER_REGISTER_SIZE);
        }
        mValuesSinceLastThreshold++;
        evaluate();
    }
    private void evaluate(){
        if(mBufferRegister.size()<BUFFER_REGISTER_SIZE){
            return;
        }

        float newValue = getAvgFromBuffer();
        mValues.add(newValue);

        if(mValuesSinceLastThreshold>=MAX_SAMPLES){
            findMinInSamples();
            findMaxInSamples();
            mThreshold = (mMinMax[1]+mMinMax[1])/2;
            mValuesSinceLastThreshold=0;
            mValues = new ArrayList<>();
        }

        if(mThreshold!=null){
            shiftSamples(newValue);
            checkIfStep();
        }
    }

    private void shiftSamples(Float newSample){
        if(mSampleNew!=null){
            mSampleOld = mSampleNew;
        }
        if(newSample>= AccelerometerDataManager.SAMPLE_PRECISION){
            mSampleNew = newSample;
        }
    }

    private boolean checkIfStep(){

        if(mSampleNew>mThreshold && mDetectionCallback!=null){
            mDetectionCallback.stepDetected(mID,mSampleNew-mSampleOld);
        }

        return false;
    }
    private float getAvgFromBuffer(){
        float result = 0f;
        for(float temp : mBufferRegister){
            result += temp;
        }
        return result/BUFFER_REGISTER_SIZE;
    }

    private void findMinInSamples(){
        float min = Float.MAX_VALUE;

        for(float temp : mBufferRegister){
            if (temp < min) min = temp;
        }
        mMinMax[0] = min;
    }
    private void findMaxInSamples(){
        float max = Float.MIN_VALUE;

        for(float temp : mBufferRegister){
            if (temp > max) max = temp;
        }
        mMinMax[1] = max;
    }
}
