package at.debugged.ms.exercise01.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import at.debugged.ms.exercise01.Global;
import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.DomainNames;
import at.debugged.ms.exercise01.services.DataService;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsFragment extends Fragment {
    @BindView(R.id.edt_first_name)
    public TextView edtFirstName;

    @BindView(R.id.edt_last_name)
    public TextView edtLastName;

    @BindView(R.id.edt_weight)
    public TextView edtWeight;

    @BindView(R.id.edt_age)
    public TextView edtAge;

    @BindView(R.id.edt_height)
    public TextView edtHeight;

    @BindView(R.id.spinner_gender)
    public Spinner genderSpinner;

    public SettingsFragment() {

        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, view);

        edtFirstName.setText(Global.currentUser.getFirstName());
        edtLastName.setText(Global.currentUser.getLastName());
        genderSpinner.setSelection(Global.currentUser.getGender());

        float height = Global.currentUser.getHeight();

        if (height != -1) {
            edtHeight.setText(String.valueOf(height));
        }

        float weight = Global.currentUser.getWeight();

        if (weight != -1) {
            edtWeight.setText(String.valueOf(weight));
        }

        int age = Global.currentUser.getAge();

        if (age != -1) {
            edtAge.setText(String.valueOf(age));
        }

        return view;
    }

    @OnClick(R.id.btn_save)
    public void onSaveClicked() {
        Global.currentUser.setFirstName(edtFirstName.getText().toString());
        Global.currentUser.setLastName(edtLastName.getText().toString());
        Global.currentUser.setGender(genderSpinner.getSelectedItemPosition());

        if (!edtHeight.getText().toString().isEmpty()) {
            Global.currentUser.setHeight(Float.valueOf(edtHeight.getText().toString()));
        }

        if (!edtWeight.getText().toString().isEmpty()) {
            Global.currentUser.setWeight(Float.valueOf(edtWeight.getText().toString()));
        }

        if (!edtAge.getText().toString().isEmpty()) {
            Global.currentUser.setAge(Integer.valueOf(edtAge.getText().toString()));
        }

        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference userReference = database.getReference(DomainNames.USER);

        userReference.child(auth.getCurrentUser().getUid()).setValue(Global.currentUser);

        Toast.makeText(getContext(), "Settings saved", Toast.LENGTH_LONG).show();
    }
}
