package at.debugged.ms.exercise01.model.domain;


import java.util.ArrayList;
import java.util.List;

public class User {
    private String firstName;
    private String lastName;
    private int age = -1;
    private float height = -1;
    private float weight = -1;
    private int gender = 0;
    private List<RunningActivity> runningActivities = new ArrayList<>();

    public User() { }

    public User(String firstName, String lastName, int age, float height, float weight, int gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.weight = weight;
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public List<RunningActivity> getRunningActivities() {
        return runningActivities;
    }

    public void setRunningActivities(List<RunningActivity> runningActivities) {
        this.runningActivities = runningActivities;
    }

    public void addRunningActivity(RunningActivity runningActivity) {
        runningActivities.add(runningActivity);
    }
}
