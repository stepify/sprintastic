package at.debugged.ms.exercise01.stepdetection.accelerometer;

import android.content.Context;
import android.util.Log;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import at.debugged.ms.exercise01.model.AccelerationListener;
import at.debugged.ms.exercise01.model.AccelerometerData;
import at.debugged.ms.exercise01.stepdetection.accelerometer.Accelerometer;

public class DummyAccelerometer extends Accelerometer {
    private Context mContext;
    private ArrayList<AccelerometerData> mValues = new ArrayList<>();

    public DummyAccelerometer(Context context) {
        mContext = context;
    }

    public void start() {
        mValues = readFromFile(mContext);

        if (mValues != null) {
            for (AccelerometerData data : mValues) {
                for (AccelerationListener listener : listeners) {
                    listener.onAccelerationChanged(data);
                }
            }
        }
    }

    private ArrayList<AccelerometerData> readFromFile(Context context) {
        FileInputStream fis = null;
        ArrayList<AccelerometerData> values = null;

        try {
            fis = context.openFileInput("accelerometer_values.dat");

            ObjectInputStream os = new ObjectInputStream(fis);
            values = (ArrayList<AccelerometerData>) os.readObject();
            os.close();
        } catch (IOException ex) {
            Log.d("TAG", ex.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return values;
    }
}
