package at.debugged.ms.exercise01.model;

import android.bluetooth.BluetoothDevice;

import java.io.Serializable;

public class BLEDeviceDescription implements Serializable {
    private BluetoothDevice mBluetoothDevice;
    private String mName;
    private String mMacAddress;
    private int mRssi;

    public BLEDeviceDescription(BluetoothDevice bluetoothDevice) {
        mBluetoothDevice = bluetoothDevice;
    }

    public String getName() {
        return mName;
    }

    public void setName(String _name) {
        mName = _name;
    }

    public String getMacAddress() {
        return mMacAddress;
    }

    public void setMacAddress(String macAddress) {
        this.mMacAddress = macAddress;
    }

    public void setRssi(int _rssi) {
        mRssi = _rssi;
    }

    public BluetoothDevice getBluetoothDevice() {
        return mBluetoothDevice;
    }

    @Override
    public String toString() {
        return mName + "\n" + mMacAddress + "         Rssi: " + mRssi;
    }
}
