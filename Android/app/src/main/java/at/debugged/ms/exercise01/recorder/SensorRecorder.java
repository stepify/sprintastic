package at.debugged.ms.exercise01.recorder;

import android.content.Context;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import at.debugged.ms.exercise01.stepdetection.accelerometer.Accelerometer;
import at.debugged.ms.exercise01.stepdetection.AccelerometerFactory;
import at.debugged.ms.exercise01.model.AccelerationListener;
import at.debugged.ms.exercise01.model.AccelerometerData;

public class SensorRecorder implements AccelerationListener {
    private Accelerometer mAccelerometer;
    private ArrayList<AccelerometerData> mValues = new ArrayList<>();
    private Context mContext;

    public SensorRecorder(Context context) {
        mContext = context;
        mAccelerometer = AccelerometerFactory.getAccelerometer(2, context);

        if (mAccelerometer != null) {
            mAccelerometer.addListener(this);
        }
    }

    public void start() {
        mAccelerometer.start();
    }

    public void stop() {
        mAccelerometer.stop();
        mAccelerometer.removeListener(this);
        writeToFile(mContext);
    }

    private void writeToFile(Context context) {
        FileOutputStream fos = null;

        try {
            fos = context.openFileOutput("accelerometer_values.dat", Context.MODE_PRIVATE);

            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(mValues);
            os.close();
        } catch (IOException ex) {
            Log.d("TAG", ex.getMessage());
        }
    }

    @Override
    public void onAccelerationChanged(AccelerometerData data) {
        mValues.add(data);
    }
}
