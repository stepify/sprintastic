package at.debugged.ms.exercise01.stepdetection.accelerometer;

import java.util.ArrayList;

import at.debugged.ms.exercise01.model.AccelerationListener;

public abstract class Accelerometer {
    public ArrayList<AccelerationListener> listeners = new ArrayList<>();

    public void addListener(AccelerationListener listener) {
        listeners.add(listener);
    }

    public void removeListener(AccelerationListener listener) {
        listeners.remove(listener);
    }

    public void start() {

    }

    public void stop() {

    }
}
