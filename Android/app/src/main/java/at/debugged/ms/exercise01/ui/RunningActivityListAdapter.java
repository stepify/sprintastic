package at.debugged.ms.exercise01.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.List;
import java.util.Locale;

import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.domain.RunningActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RunningActivityListAdapter extends ArrayAdapter<RunningActivity> {

    private Context context;
    private List<RunningActivity> activities;

    @BindView(R.id.txt_start_time)
    TextView txtStartTime;

    @BindView(R.id.txt_end_time)
    TextView txtEndTime;

    @BindView(R.id.txt_distance)
    TextView txtDistance;

    public RunningActivityListAdapter(Context context, List<RunningActivity> activities) {
        super(context, -1, activities);

        this.context = context;
        this.activities = activities;
    }

    @NonNull
    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_activities_list, parent, false);

        ButterKnife.bind(this, view);

        RunningActivity current = activities.get(activities.size() - 1 - position);

        DateTimeFormatter dtf = DateTimeFormat.shortDateTime();
        txtStartTime.setText(dtf.print(current.getStartDateTime()));
        txtEndTime.setText(dtf.print(current.getEndDateTime()));
        txtDistance.setText(String.format(Locale.getDefault(), "%1$,.2f", current.getDistance() / 1000));

        return view;
    }
}
