package at.debugged.ms.exercise01.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.content.ContextCompat;

public class PermissionUtils {
    public static boolean checkBluetoothAdmin(Context _context) {
        int permissionCheck = ContextCompat.checkSelfPermission(_context, Manifest.permission.BLUETOOTH_ADMIN);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkFineLocation(Context _context) {
        int permissionCheck = ContextCompat.checkSelfPermission(_context, Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkInternet(Context _context) {
        int permissionCheck = ContextCompat.checkSelfPermission(_context, Manifest.permission.INTERNET);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }
}

