package at.debugged.ms.exercise01.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import at.debugged.ms.exercise01.Global;
import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.BLEDeviceDescription;
import at.debugged.ms.exercise01.model.domain.RunningActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ActivityOverviewFragment extends Fragment {

    private List<RunningActivity> mRunningActivities = new ArrayList<>();
    private RunningActivityListAdapter mActivitiesListAdapter;

    @BindView(R.id.lv_former_activities)
    ListView lvFormerActivities;

    public ActivityOverviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start_activity, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        mRunningActivities = Global.currentUser.getRunningActivities();

        mActivitiesListAdapter = new RunningActivityListAdapter(getContext(), mRunningActivities);
        lvFormerActivities.setAdapter(mActivitiesListAdapter);
    }

    @OnClick(R.id.start_button)
    public void onStartNewActivityClicked() {
        startActivity(new Intent(getActivity(), WorkoutActivity.class));
    }
}
