package at.debugged.ms.exercise01.utils;

import android.content.Context;
import android.util.TypedValue;

public class UnitConverter {
    public static float dpToPx(Context context, int dp) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }
}
