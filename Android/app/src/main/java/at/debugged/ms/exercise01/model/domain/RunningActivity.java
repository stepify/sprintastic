package at.debugged.ms.exercise01.model.domain;

import com.google.firebase.database.Exclude;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import at.debugged.ms.exercise01.model.WayPoint;

public class RunningActivity {
    private String name;
    private ArrayList<WayPoint> route;
    private ArrayList<HeartRateInfo> heartRateInfos;
    private ArrayList<Double> calories;
    private ArrayList<Double> vomax;
    private Date startTime;
    private Date endTime;
    private double distance = 0d;
    private long steps;

    public RunningActivity() {
        // required no argument constructor for firebase
        route = new ArrayList<>();
        heartRateInfos = new ArrayList<>();
        calories = new ArrayList<>();
        vomax = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Collection<WayPoint> getRoute() {
        return route;
    }

    public void setRoute(ArrayList<WayPoint> route) {
        this.route = route;
    }

    public void addWayPoint(WayPoint wayPoint) {
        route.add(wayPoint);
    }

    public void addWayPoints(Collection<WayPoint> wayPoints) {
        route.addAll(wayPoints);
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public void addHeartRAteInfo(HeartRateInfo heartRateInfo) {
        heartRateInfos.add(heartRateInfo);
    }

    public ArrayList<HeartRateInfo> getHeartRateInfos() {
        return heartRateInfos;
    }

    public void setHeartRateInfos(ArrayList<HeartRateInfo> heartRateInfos) {
        this.heartRateInfos = heartRateInfos;
    }

    @Exclude
    public DateTime getStartDateTime() {
        return new DateTime(startTime);
    }

    @Exclude
    public void setStartDateTime(DateTime startDateTime) {
        startTime = startDateTime.toDate();
    }

    @Exclude
    public DateTime getEndDateTime() {
        return new DateTime(endTime);
    }

    @Exclude
    public void setEndDateTime(DateTime endDateTime) {
        endTime = endDateTime.toDate();
    }

    @Override
    public String toString() {
        return "Activity: " + route.size();
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double _distance) {
        distance = _distance;
    }

    public long getSteps() {
        return steps;
    }

    public void setSteps(long _steps) {
        steps = _steps;
    }

    public void addCalorieInfo(Double currentBurnedCalories) {
        calories.add(currentBurnedCalories);
    }

    public void addVo2MaxInfo(Double vo2max) {
        vomax.add(vo2max);
    }

    public ArrayList<Double> getCalories() {
        return calories;
    }

    public void setCalories(ArrayList<Double> calories) {
        this.calories = calories;
    }

    public ArrayList<Double> getVomax() {
        return vomax;
    }

    public void setVomax(ArrayList<Double> vomax) {
        this.vomax = vomax;
    }
}
