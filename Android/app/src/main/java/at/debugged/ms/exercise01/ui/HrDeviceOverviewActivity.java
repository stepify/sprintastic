package at.debugged.ms.exercise01.ui;

import android.app.Activity;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.model.BLEDeviceDescription;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class HrDeviceOverviewActivity extends AppCompatActivity {

    private static final String TAG = "HrDeviceOverview";
    private static final long SCAN_DURATION_MILLIS = 4000;

    private BluetoothLeScanner mBluetoothLeScanner;
    private List<ScanResult> mScanResults;

    private List<BLEDeviceDescription> mBLEDeviceList = new ArrayList<>();
    private ArrayAdapter<BLEDeviceDescription> bleDevicesListAdapter;

    @BindView(R.id.lv_bluetooth_devices)
    ListView mLvBluetoothDevices;

    ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            Log.d(TAG, "Device found: " + result);

            for (ScanResult scanResult : mScanResults) {
                if (scanResult.getDevice().equals(result.getDevice())) {
                    return;
                }
            }

            mScanResults.add(result);

            BLEDeviceDescription description = new BLEDeviceDescription(result.getDevice());
            description.setName(result.getDevice().getName());
            description.setRssi(result.getRssi());
            description.setMacAddress(result.getDevice().getAddress());

            mBLEDeviceList.add(description);
            bleDevicesListAdapter.notifyDataSetChanged();
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hrdevices_overview);

        ButterKnife.bind(this);

        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothLeScanner = bluetoothManager.getAdapter().getBluetoothLeScanner();

        bleDevicesListAdapter = new ArrayAdapter<>(this, R.layout.item_device_list, R.id.txt_device_name, mBLEDeviceList);
        mLvBluetoothDevices.setAdapter(bleDevicesListAdapter);

        startScan();
    }

    private void startScan() {
        Log.d(TAG, "BLE Scan started");

        ScanSettings.Builder settings = new ScanSettings.Builder();
        settings.setScanMode(ScanSettings.SCAN_MODE_BALANCED);
        mBluetoothLeScanner.startScan(null, settings.build(), mScanCallback);

        mScanResults = new ArrayList<>();

        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, SCAN_DURATION_MILLIS);
    }

    private void stopScan() {
        Log.d(TAG, "BLE Scan stopped");
        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mBluetoothLeScanner.stopScan(mScanCallback);
    }

    @OnItemClick(R.id.lv_bluetooth_devices)
    public void onDeviceSelected(int position) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("selected_hr_sensor", mBLEDeviceList.get(position).getBluetoothDevice());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
