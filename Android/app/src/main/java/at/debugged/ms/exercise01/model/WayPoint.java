package at.debugged.ms.exercise01.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Martin on 28.01.2018.
 */

public class WayPoint {
    private double lat,lng;

    public WayPoint() {
        // required
    }

    public WayPoint(double _lat, double _lng){
        this.lat = _lat;
        this.lng = _lng;
    }
    public WayPoint(Location _location){
        this.lat = _location.getLatitude();
        this.lng = _location.getLongitude();
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double _lat) {
        lat = _lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double _lng) {
        lng = _lng;
    }

    public void setLocation(Location _location) {
        this.lat = _location.getLatitude();
        this.lng = _location.getLongitude();
    }

    public LatLng toLatLng() {
        return new LatLng(lat, lng);
    }
}
