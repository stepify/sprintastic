package at.debugged.ms.exercise01.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import at.debugged.ms.exercise01.R;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Philipp on 18.12.2017.
 */

public class StepDialog extends DialogFragment {

    private SharedPreferences mSharedPref;
//    @BindView(R.id.stepGoal) EditText mEditGoal;
    EditText mEditSteps;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.step_dialog, null)).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mSharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                mEditSteps = (EditText) getDialog().findViewById(R.id.stepGoal);
                SharedPreferences.Editor editor = mSharedPref.edit();
                editor.putInt(getString(R.string.preference_set_step_goal),Integer.valueOf(mEditSteps.getText().toString()));
                editor.commit();
                Toast.makeText(getActivity(),"New Goal saved!",Toast.LENGTH_SHORT).show();
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                StepDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }


    public void run()
    {
        Intent intent = getActivity().getIntent();
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_NO_ANIMATION);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();

        getActivity().overridePendingTransition(0, 0);
        startActivity(intent);
    }
}
