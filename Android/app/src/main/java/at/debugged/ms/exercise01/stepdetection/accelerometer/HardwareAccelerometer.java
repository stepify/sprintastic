package at.debugged.ms.exercise01.stepdetection.accelerometer;


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import at.debugged.ms.exercise01.model.AccelerationListener;
import at.debugged.ms.exercise01.model.AccelerometerData;
import at.debugged.ms.exercise01.stepdetection.accelerometer.Accelerometer;

import static android.content.Context.SENSOR_SERVICE;

public class HardwareAccelerometer extends Accelerometer implements SensorEventListener {
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    public HardwareAccelerometer(Context context) {
        mSensorManager = (SensorManager)context.getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void start() {
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void stop() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.equals(mAccelerometer)) {
            AccelerometerData data = new AccelerometerData();
            data.setX(sensorEvent.values[0]);
            data.setY(sensorEvent.values[1]);
            data.setZ(sensorEvent.values[2]);

            for (AccelerationListener listener : listeners) {
                listener.onAccelerationChanged(data);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
