package at.debugged.ms.exercise01.services;


import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import java.util.UUID;

import at.debugged.ms.exercise01.R;
import at.debugged.ms.exercise01.ui.HrDeviceOverviewActivity;

import static at.debugged.ms.exercise01.Tags.ACTIVITY_MESSENGER_EXTRA;
import static at.debugged.ms.exercise01.Tags.HEART_RATE_CHANGED;
import static at.debugged.ms.exercise01.Tags.PREF_SERVICE_ALIVE;
import static at.debugged.ms.exercise01.Tags.UPDATE_ACTIVITY_MESSENGER;

public class BLEService extends Service {

    private static final String TAG = "BLEService";
    private static final int NOTIFICATION_ID = 1;

    private BluetoothGatt mBluetoothGattConnection;

    private Messenger mActivityMessenger;
    private Messenger mServiceMessenger;

    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            Log.d(TAG, "Message received: " + msg);

            switch (msg.what) {
                case UPDATE_ACTIVITY_MESSENGER:
                    Log.d(TAG, "Messenger connection established!");
                    break;
            }
            return true;
        }
    });

    private BluetoothGattCallback mBluetoothGattCallback = new BluetoothGattCallback() {
        UUID HEART_RATE_SERVICE = UUID.fromString("0000180d-0000-1000-8000-00805f9b34fb");
        UUID HEART_RATE_MEASUREMENT_CHARACTERISTIC = UUID.fromString("00002a37-0000-1000-8000-00805f9b34fb");
        UUID CLIENT_CHARACTERISTIC_CONFIG = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

        @Override
        public void onConnectionStateChange(BluetoothGatt _gatt, int _status, int _newState) {
            if (_newState == BluetoothProfile.STATE_CONNECTED) {
                Log.i(TAG, "onConnectionStateChange - connected to device");
                mBluetoothGattConnection = _gatt;
                mBluetoothGattConnection.discoverServices();
            } else {
                Log.i(TAG, "onConnectionStateChange - disconnected from device");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt _gatt, int _status) {
            Log.i(TAG, "onConnectionStateChange - services discovered");

            BluetoothGattService gattService = _gatt.getService(HEART_RATE_SERVICE);

            if (gattService != null) {
                BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(HEART_RATE_MEASUREMENT_CHARACTERISTIC);

                if (gattCharacteristic != null) {
                    mBluetoothGattConnection.setCharacteristicNotification(gattCharacteristic, true);

                    mBluetoothGattConnection.readCharacteristic(gattCharacteristic);

                    BluetoothGattDescriptor gattDescriptor = gattCharacteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG);
                    gattDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    mBluetoothGattConnection.writeDescriptor(gattDescriptor);
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt _gatt, BluetoothGattCharacteristic _characteristic) {
            if (_characteristic.getUuid().equals(HEART_RATE_MEASUREMENT_CHARACTERISTIC)) {
                // This is special to heart rate measurement
                // If the Bit 0 is 0, then the format is an unsigned int with 8 bytes
                int flag = _characteristic.getProperties();
                int format = -1;

                if ((flag & 0x01) != 0) {
                    format = BluetoothGattCharacteristic.FORMAT_UINT16; //Heart rate UINT16}
                } else {
                    format = BluetoothGattCharacteristic.FORMAT_UINT8; //Heart rate UINT8
                }

                int heartRate = _characteristic.getIntValue(format, 1);

                visualizeData(heartRate);
                //communicateData(heartRate);
                Log.d(TAG, String.valueOf(heartRate));
            }
        }
    };

    private void visualizeData(int hearRate) {
        Message msg = Message.obtain();
        msg.what = HEART_RATE_CHANGED;
        msg.arg1 = hearRate;

        try {
            mActivityMessenger.send(msg);
        } catch (RemoteException _e) {
            _e.printStackTrace();
        }
    }

    private void communicateData(int heartRate) {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "creating service...");

        mServiceMessenger = new Messenger(mHandler);

        fireNotification();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences.edit().putBoolean(PREF_SERVICE_ALIVE, true).apply();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mActivityMessenger = intent.getParcelableExtra(ACTIVITY_MESSENGER_EXTRA);
        BluetoothDevice hrSensor = intent.getParcelableExtra("hr_sensor");

        // send my messenger to activity
        Message msg = Message.obtain();
        msg.what = UPDATE_ACTIVITY_MESSENGER;
        msg.replyTo = mServiceMessenger;

        try {
            mActivityMessenger.send(msg);
        } catch (RemoteException _e) {
            _e.printStackTrace();
        }

        hrSensor.connectGatt(getApplicationContext(), true, mBluetoothGattCallback);

        return Service.START_STICKY;
    }

    private void fireNotification() {
        Intent resultIntent = new Intent(this, HrDeviceOverviewActivity.class);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(
                this,
                0,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(android.R.mipmap.sym_def_app_icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Hello World!")
                .setContentIntent(resultPendingIntent)
                .build();

        startForeground(NOTIFICATION_ID, notification);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferences.edit().putBoolean(PREF_SERVICE_ALIVE, false).apply();

        if (mBluetoothGattConnection != null) {
            mBluetoothGattConnection.disconnect();
            mBluetoothGattConnection.close();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent _intent) {
        return null;
    }
}
