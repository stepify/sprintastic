# Sprintastic
developed during the course mobile sports at the University of Applied Sciences Hagenberg  
by Philip Graf, Michael Rockenschaub and Martin Schneglberger

## Tasks
#### Step detection and counter.
Steps are detected and counted by the algorithm shown in the lecture as well as Android's virtual step sensor.   
Visualization is done inside the application in an own Activity, since steps are also counted in the background

#### Database connection
All data gathered by the mobile application is stored in the Firebase Database, which is a "Server as a Service".    
Thus, all data is stored as key-value pairs.

#### GPS and Strive length
During a run, the total distance is measured by constantly fetching the location from the GPS sensor of the device via a foreground service and calculating the distances between with trigonometry. These points are also used in order to update the Google Map of the *Running Activity* with the current route.     
In order not to depend on the availability of GPS, the distance can also be calculated by the strive length combined with the total amount of steps made during the run. The strive length depends on various metrics the user has to provide upon registration.

#### Heart-rate-related extensions
done in mobile application

#### Calculation of energy expenditure from heart-rate
done in mobile application

#### Equivalent pace; SRTM-corrected altitude or barometric pressure
done in mobile application with the help of Google Elevation API

#### ConnectIQ app
a stopwatch with the functions "start", "pause", "stop", and "reset" has been implemented.

#### Prediction of finishing times
equivalent solution done in Web application -> predicting maximum repetitions

#### Activity classification
done in Weka

#### Web application
The web application uses the Angular 5 framework together with Bootstrap to define the UI.    
RxJS is imported to enable the use of AngularFire (Framework to use Firebase in Angular Applications) according to the reactive functional programming pattern.     
The web application allows user login and registration. After a successful authentication, the user sees a protocol of his/her last runs with a short summary of each. This section describes the main section of the web application. When selecting one of the past runs, the user can take detailed insight into his/her activity. The detail page lists all data collected during the run and graphically visualizes the route of the run on a Google Map (if GPS was active during the run).   

#### One Repetition Calculation
The web application also includes the possibility to predict the maximum weight a person can move for one complete repetition for various weight lifting exercises.    
This was done my taking insight into the papers written by various sports scientists. In order to compare them, the calculations were altered in a way that they can also be used to calculated the maximum weight for multiple repetitions. The results of these calculations are then drawn into a line chart.   
Users can use a number of repetitions together with the used weight as inputs. When altering, the calculations are made again and the chart is updated.   
The method which works best in our opinion is the one defined by *MAYHEW et al*.
